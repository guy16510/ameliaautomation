const puppeteer = require('puppeteer');
const domElements = require('./constants/domElements.json');
const response = require('./testCases/Car-1_Driver-1_Accident-1.json');

(async () => {
  const browser = await puppeteer.launch({
    //set to true to stop chrome from displaying
    headless: domElements.Headless
  });

  let page = await browser.newPage();

  // Checks chat if it is done loading Text
  // True if its done
  // False if it is not.
  async function didAmeliaRespond(){
    if (await page.$(domElements.Div_Loading) === null){
      return true;
    }
    else{
      return false;
    }
  }

  // Enter text and it checks if
  async function enterText(typeText) {
    let pageComplete = await didAmeliaRespond();
    if(pageComplete){
      await page.click(domElements.ChatDiv);
      await page.keyboard.type(typeText);
      await page.keyboard.press(String.fromCharCode(13)); //hit enter
      await page.waitFor(2*1000);
    }
    else{
      await page.waitFor(1000);
      await enterText(typeText); //recurse
    }
  }

  //Finds button from listing, and clicks it.
  async function clickButtonVal(buttonValName) {
    let pageComplete = await didAmeliaRespond();
    if(pageComplete) {
      await page.waitFor(500);
      const hrefs = await page.evaluate(() => {
        const anchors = document.querySelectorAll("h6.SelectButton-name");
        return [].map.call(anchors, a => a.textContent);
      });

      //have location of element.
      for(let i=0; i< hrefs.length; i++){
        if(hrefs[i].toUpperCase() === buttonValName.toUpperCase()){
          await page.click("h6.Select-wrapper > div > div:nth-child("+ (i + 1) +")");
          await page.waitFor(2*1000);
        }
      }
    }
    else{
      await page.waitFor(1000);
      await clickButtonVal(buttonValName); //recurse
    }
  }

  //Finds button from listing, and clicks it.
  async function clickGenderButton(buttonValName) {
    let pageComplete = await didAmeliaRespond();
    if(pageComplete) {

      await page.waitFor(500);
      if(buttonValName.toUpperCase() === "MALE"){
        await page.click('#GenderSectionField0');
        await page.waitFor(2*1000);
      }
      else{
        await page.click('#GenderSectionField1');
        await page.waitFor(2*1000);
      }
    }
    else{
      await page.waitFor(1000);
      await clickButtonVal(buttonValName); //recurse
    }
  }

  // creates array of all correspondence. Prints last thing in array
  async function getAmeliaResponse() {
    const AmeliaResponse = await page.evaluate(() => {
      const tds = Array.from(document.querySelectorAll('.message-chat.transition-done.message-chat--amelia h6 div'));
      return tds.map(td => td.textContent)
    });

    console.log(AmeliaResponse[AmeliaResponse.length -1]); //print last thing she said.
  }

  async function getQuoteId() {
    const AmeliaResponse = await page.evaluate(() => {
      const tds = Array.from(document.querySelectorAll('#root > div > div.main > div.ChatNotes.active > div > div > div > div.Banner-wrapper > div.Banner-item.Banner-font-size-title.Banner-font-weight-normal > span'));
      let finalArr = tds.map(td => td.textContent);
      return finalArr[0];
    });
    console.log(AmeliaResponse); //print last thing she said.
    //remove ID
    const quote = AmeliaResponse.split("#")[1];
    const quoteFinal = quote.split(" ")[0];
    return quoteFinal || 'undefined';
  }

  async function processJson(iNum){
    if(iNum != response.Questions.length){
      let buttonOrText = response.Questions[iNum];
      if(buttonOrText.type === "options"){
        await clickButtonVal(buttonOrText.response);
      }
      else if(buttonOrText.type === "optionsMaleFemale"){
        await clickGenderButton(buttonOrText.response);
      }
      else{
        await enterText(buttonOrText.response);
      }
      let nextOne = iNum + 1;
      return processJson(nextOne);
    }
    else{
      return; //its completed
    }
  }

  await page.goto(domElements.Url);
  await page.setViewport({width:1280, height:816});
  await page.waitForSelector(domElements.AmeliaBubbleDiv); // wait for bubble message to appear then start hammering away.
  await enterText(response.Start); // this might not be necessary down the road.
  await page.click(domElements.CloseX);
  await page.waitForSelector(domElements.ChatDiv); // wait for chat dialog to load

  await processJson(0); //processes questions
  await page.screenshot({path: './browserImages/Quote_' + await getQuoteId() + '.png'});
  browser.close();
})();