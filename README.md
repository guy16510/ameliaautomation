# Amelia Automation

### Installation
        git clone https://guy16510@bitbucket.org/guy16510/ameliaautomation.git
        cd ameliaautomation
        npm install
        npm start

## Code overview
- This application uses Puppeteer as a headless framework
- The testCases are in their own folder ./testCases/*
    - The Questions Array Object, goes in order of each question.
- To run different test cases, change the "response" in server.js to a different file.
- On completion, this will take an image of the browser and save it to ./browserImages/* with a Quote ID as the name


This is a rough working of it. Many things could be added to enhance the repo. Just ping me if you have any suggestions.
